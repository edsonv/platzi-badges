import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Home from '../pages/Home';
import Layout from '../components/Layout';
import Badges from '../pages/Badges';
import BadgesNew from '../pages/BadgeNew';
import BadgeEdit from '../pages/BadgeEdit';
import NotFound from '../pages/NotFound';
import BadgeDetails from '../pages/BadgeDetailsContainer';

function App() {
    return (
        <BrowserRouter basename='/platzi-badges'>
            <Layout>
                <Switch>
                    <Route exact path='/' component={Home} />
                    <Route exact path='/badges' component={Badges} />
                    <Route exact path='/badges/new' component={BadgesNew} />
                    <Route
                        exact
                        path='/badges/:badgeId'
                        component={BadgeDetails}
                    />
                    <Route
                        exact
                        path='/badges/:badgeId/edit'
                        component={BadgeEdit}
                    />
                    <Route component={NotFound} />
                </Switch>
            </Layout>
        </BrowserRouter>
    );
}

export default App;
